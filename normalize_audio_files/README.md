# Scripts
## Normalize Audio File Names

This script uses `kid3-cli` to rename audio files with the pattern "Artist - Title.Extension" for consistency in an audio library.

**WARNING**: your audio library has to be in a format supported by `kid3`, look at [https://kid3.kde.org/](their home page)

![Screenshot](screenshot.png)

### Requirements
```
a POSIX shell
a terminal with colors support
kid3-cli
```

### Usage
```sh
normalize_audio_files [extension]

    Options:
        extension music files extension.
```

Executing without options works with .opus files

### Notes
This script has only been tested with GNU/linux and bash
but it should work with any operating system meeting the requirements
