# Scripts
## Kcal

A Simple shell script for managing your daily intake in kilocalories, with a graph feature (termgraph)

![Screenshot](screenshot.png)

### Requirements
```
a POSIX shell
a terminal with colors support
[only for -g] python 3.5+ and termgraph
```

### Usage
```sh
kcal [-a value | -d value | -g]

    Options:
        -a add a value for today    (in kcal)
        -d remove a value for today (in kcal)
        -g display statistics for the 7 past saved days
```

Executing without options prints your daily intake

### Notes
This script has only been tested with GNU/linux and bash
but it should work with any operating system meeting the requirements
