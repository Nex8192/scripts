# Scripts
## Ringtone generator

Generates simple ringtone sounds

### Requirements
```
a POSIX shell
sox' play command
```

### Usage
```sh
ringtone_generator example.ring
```

### Ring file
The ringtone is describe in a simple file in the format

```
bpm|note1:duration1 note2:duration2 …
```

Where

- `bpm` is the speed in beats per minute
- each `note` is the musical note (in [Scientific pitch notation](https://en.wikipedia.org/wiki/Scientific_pitch_notation))
- each `duration` is the duration for that note, relative to the quarter note/crotchet duration defined by `bpm`.
  (e.g. 1 is a quarter note, 4 is a semibreve/whole note)

### Notes
This script has only been tested on linux with zsh and ash
but it should work with any operating system meeting the requirements

I made this script to generate simple ringtones for calls and SMS on my pinephone with SXMO, and I call it in $XDG\_CONFIG\_HOME/sxmo/hooks/ring and/or in $XDG\_CONFIG\_HOME/sxmo/hooks/sms
