# Scripts
## Youtube play

A Simple shell script which uses youtube-dl to play sound or videos

![Screenshot](screenshot.png)

### Requirements
```
a POSIX shell
youtube-dl
ffmpeg + ffplay
```

### Usage
```sh
youtube-play "youtube query" [1]

    Options:
        1 enables the video mode (default is audio-only)
```

### Notes
This script has only been tested with GNU/linux and bash
but it should work with any operating system meeting the requirements
